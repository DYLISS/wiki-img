# wiki-img

## Description
It is a Docker image to manage wikis. It was written by Meziane AITE five years ago.

## Installation
`docker pull registry.gitlab.inria.fr/dyliss/wiki-img/wiki-img:latest`

